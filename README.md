# Arcadia

Arcadia is a basic microservice that can be used to retrieve information about any Summoner of League of Legends.
This implementation uses some RiotWatch and Cassiopeia which are most used python package to fetch information from Riot Endpoints.

# Service Design
This service is splitted in two core folders that contains every logic that is required to fetch information.
 - api/     :   Contains every Django to run a new HTTP Server. Every API has use of `arcadia` package to generate and apply the required logic.
 - src/     :   This folder contains internal `arcadia` package which uses Domain Driven Design to bring greater scalability in future implementations.

## Installation
This application is dockerized, to run this microservices run:
```docker-compose up```

*Note: Most used commands are setted in Makefile, making it the entrypoint of the whole application.*

## Author
Cristopher Adasme (cadasmeq@gmail.com)
