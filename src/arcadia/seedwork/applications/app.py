from arcadia.seedwork.applications.commands import Command
from arcadia.seedwork.applications.queries import Query


class Application:
    commands: Command
    queries: Query
