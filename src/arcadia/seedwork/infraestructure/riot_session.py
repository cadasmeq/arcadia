import os

import cassiopeia
from riotwatcher import LolWatcher

RIOT_API_KEY = os.getenv("RIOT_API_KEY") or None


def new_lolwatcher_session() -> LolWatcher:
    """Returns a new Lolwatcher instance

    Returns:
        [LolWatcher]: Lolwatcher instance that can be used to retrieves data
    """
    return LolWatcher(RIOT_API_KEY)


def new_cassiopeia_session() -> cassiopeia:
    """Returns a new cassiopeia instance

    Returns:
        [cassiopeia]: cassiopeia instance that can be used to retrieves data
    """
    cassiopeia.set_riot_api_key(RIOT_API_KEY)
    return cassiopeia
