from .exceptions import BusinessRuleValidationException
from .rules import BusinessRule


class BusinessRuleValidationMixin:
    """
    class carry any rule or validation which a services require
    """

    def check_rule(self, rule: BusinessRule):
        if rule.is_broken():
            raise BusinessRuleValidationException(rule.message)
