from typing import Any, Protocol


class Repository(Protocol):
    """Domain Repository Interface"""

    client: Any

    def get_by_uuid(self):
        ...
