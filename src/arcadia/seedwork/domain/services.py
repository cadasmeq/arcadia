from .mixins import BusinessRuleValidationMixin


class DomainService(BusinessRuleValidationMixin):
    """
    Domain Services carry domain knowledge that
    doesnt naturally fit entities and value objects
    """

    ...
