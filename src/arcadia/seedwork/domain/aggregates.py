from dataclasses import dataclass

from .entities import Entity
from .mixins import BusinessRuleValidationMixin


@dataclass
class AggregateRoot(BusinessRuleValidationMixin, Entity):
    """Consits of 1+ entities."""

    ...
