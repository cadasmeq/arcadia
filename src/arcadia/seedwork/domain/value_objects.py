import uuid
from typing import Protocol


class ValueObject(Protocol):
    """
    Base class for value objects
    """

    ...


class UUID(uuid.UUID):
    """UUID Class

    Brings for differents version of uuid
    """

    def v4(self):
        return uuid.uuid4()
