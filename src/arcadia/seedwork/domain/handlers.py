from typing import Protocol

from arcadia.seedwork.domain.repositories import Repository


class DomainHandler(Protocol):
    """
    Domain handler

    + repository (Repository) : A handler use a repository in order to retrieve information.
    """

    repository: Repository

    def handle(self):
        ...
