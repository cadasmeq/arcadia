from typing import Protocol


class BusinessRule(Protocol):
    """Rules base class for every domain business rule"""

    message: str

    def is_broken(self) -> bool:
        pass
