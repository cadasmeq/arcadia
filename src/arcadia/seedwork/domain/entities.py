from dataclasses import dataclass, field
from typing import Any

from .value_objects import UUID


@dataclass
class Entity:
    id: UUID = field(hash=True)

    @classmethod
    def next_id(cls) -> Any:
        return UUID().v4()
