..DEFAULT_GOAL := help


.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

### DOCKER ###
.PHONY: up
up: ## [docker] run the project
	@docker-compose run --service-ports --rm backend || true

.PHONY: stop
stop: ## [docker] stop Docker containers without removing them
	@docker-compose stop

.PHONY: down
down: ## [docker] stop and remove Docker containers
	@docker-compose down --remove-orphans

.PHONY: rebuild
rebuild: ## [docker] rebuild base Docker images
	@docker-compose down --remove-orphans
	@docker-compose build --no-cache

.PHONY: reset
reset: ## [docker] update Docker images and reset local databases
	@docker-compose down --volumes --remove-orphans
	@docker-compose pull

.PHONY: pull
pull: ## [docker] update Docker images without losing local databases
	@docker-compose down --remove-orphans
	@docker-compose pull

.PHONY: fromscratch
fromscratch: reset pull up


### DEVELOPMENT SHORTCUTS ###
.PHONY: initdev
initdev: create-env install-deps install-precommit ## [development] creates a virtualenv and install development dependencies

.PHONY: create-env
create-env: ## [development] initialize locally a new virtual env
	python3 -m venv .venv || true
	. .venv/bin/activate

.PHONY: install-deps
install-deps: ## [development] install dependencies
	pip3 install -r requirements/dev.txt

.PHONY: install-precommit
install-precommit :## [development] install precommit hooks
	pre-commit install

.PHONY: tests
tests:  ## [development] applies flake8/pytest/mypy to api/ and src/
	pytest tests || true
	flake8 api src tests || true
	mypy src tests || true

.PHONY: format
format: ## [development] applies isort/black to api/ and src/
	@isort src/ tests/
	@black src/ tests/


### DJANGO ###
.PHONY: runserver
runserver: ## [django] runs a django server
	@python3 manage.py makemigrations
	@python3 manage.py migrate
	@python3 manage.py runserver

.PHONY: shell_plus
shell_plus:  ## [django] runs django shell plus
	@python3 manage.py shell_plus
